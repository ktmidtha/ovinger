# Lag et program som gjør følgende:
# 1) Spør bruker hvor mye hun har på brukskonto.
# 2) Spør bruker hvor mye hun har på sparekonto.
# 3) Skriver ut hvor mye brukeren har totalt på brukskonto og sparekonto.

brukskonto = int(input("Hvor mye har du på brukskonto: "))
sparekonto = int(input("Hvor mye har du på sparekonto: "))

total_konto = brukskonto + sparekonto
print("Du har", total_konto, "kr på konto (totalt)")